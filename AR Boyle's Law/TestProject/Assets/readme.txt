Representation of Boyle's Law with the power of AR
Print the image in the Project Files /Assets/Chem/Tracker.png
The image would act as a Tracker for the AR Camera to Track
Once that is done, simply play and your webcam should turn on. Aim it on your Tracker and it should Render the Baloon model
There are 2 sliders. 1 for the Volume and 1 or the Pressure control.
By default you can only modify the value of the Volume Slider. In order to modify the value of the Pressure, tick the check box right next to the Pressure Slider.

This Unity file can be exported to an APK and can be run on Android Devices as well

The program was made for a school science exhibition where AR was one of the topic.
Interestingly enough I was the only student (as far as I know) to take up AR as one of the Topics.
The Topic represents Boyle's Law with the help of AR (powered by Vuforia)
Boyle's Law is an experimental gas law that describes how the pressure of a gas tends to increase as the volume of the container decreases. [Wikipedia]
