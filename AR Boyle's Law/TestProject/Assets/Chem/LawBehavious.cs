﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LawBehavious : MonoBehaviour
{
    public TextMeshProUGUI Vol, Pres;
    public Slider sliderVol;
    public Slider sliderPres;
    public bool ChangingVol;
    public Toggle toggle;

    // Update is called once per frame
    void Update()
    {
        ChangingVol = toggle.isOn;
        Vol.text = sliderVol.value.ToString();
        Pres.text = sliderPres.value.ToString();
        if (ChangingVol)
        {
            sliderVol.value = 1 / (sliderPres.value / 25f);
        }
        else
        {
            sliderPres.value = (1 / (sliderVol.value / 0.2f))*125f;
        }
       
        transform.localScale = new Vector3(sliderVol.value, sliderVol.value, sliderVol.value) / 1.35f;

    }
}
